document.addEventListener('DOMContentLoaded', function() {
    // Elimina esta llamada, ya que loadTasks() se llama dentro de $(document).ready()
    // loadTasks();
});

const base_url = 'http://localhost:8000/api/';

function showDeleteModal(id) {
    document.getElementById('deleteTaskId').value = id;
    document.getElementById('confirmDelete').setAttribute('onclick', `deleteTask(${id})`);

    $('#deleteModal').modal('show');
}

function deleteTask(id) {
    fetch(`${base_url}task/${id}/`, {
        method: 'DELETE'
    })
    .then(response => {
        if (response.status === 204) {
            $('#deleteModal').modal('hide');
            location.reload(); // Recarga la página para mostrar los cambios
        } else {
            alert('Error al eliminar la tarea');
        }
    })
    .catch(error => console.error('Error al eliminar tarea:', error));
}

function showEditModal(id, title, description, resolved) {
    document.getElementById('editTaskId').value = id;
    document.getElementById('editTaskTitle').value = title;
    document.getElementById('editTaskDescription').value = description;
    document.getElementById('editTaskResolved').checked = resolved;

    $('#editModal').modal('show');
}

function updateTask() {
    const id = document.getElementById('editTaskId').value;
    const title = document.getElementById('editTaskTitle').value;
    const description = document.getElementById('editTaskDescription').value;
    const resolved = document.getElementById('editTaskResolved').checked;

    const data = {
        id: id,
        title: title,
        description: description,
        resolved: resolved
    };

    fetch(`${base_url}task/${id}/`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.ok) {
            $('#editModal').modal('hide');
            location.reload(); // Recarga la página para mostrar los cambios
        } else {
            throw new Error('Error al actualizar la tarea');
        }
    })
    .catch(error => {
        console.error('Error al actualizar tarea:', error);
        alert('Error al actualizar la tarea. Verifica la consola para más detalles.');
    });
}

function createTask() {
    const title = document.getElementById('createTaskTitle').value;
    const description = document.getElementById('createTaskDescription').value;
    const resolved = document.getElementById('createTaskResolved').checked;
    const user_id = document.getElementById('createTaskUser').value;

    const data = {
        title: title,
        description: description,
        resolved: resolved,
        user_id: user_id
    };

    fetch(`${base_url}tasks/create/`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.ok) {
            $('#createModal').modal('hide');
            location.reload(); // Recarga la página para mostrar los cambios
        } else {
            throw new Error('Error al crear la tarea');
        }
    })
    .catch(error => {
        console.error('Error al crear tarea:', error);
        alert('Error al crear la tarea. Verifica la consola para más detalles.');
    });
}

function loadTasks() {
    const taskTable = document.getElementById('task-table').getElementsByTagName('tbody')[0];

    fetch(`${base_url}tasks/`)
        .then(response => response.json())
        .then(tasks => {
            taskTable.innerHTML = ''; // Limpiar contenido existente antes de agregar nuevas filas

            tasks.forEach(task => {
                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${task.id}</td>
                    <td>${task.title}</td>
                    <td>${task.description}</td>
                    <td>${task.resolved ? 'Terminado' : 'Pendiente'}</td>
                    <td>
                        <div class="d-flex justify-content-start">
                            <button type="button" class="btn btn-primary btn-sm mr-2" onclick="showEditModal(${task.id}, '${task.title}', '${task.description}', ${task.resolved})">Editar</button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="showDeleteModal(${task.id})">Eliminar</button>
                        </div>
                    </td>
                `;
                taskTable.appendChild(row);
            });
        })
        .catch(error => console.error('Error al cargar tareas:', error));
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    
    // Cargar todas las tareas al inicio
    loadTasks();
    
    // Filtrar tareas por estado
    $('#btnAllTasks').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        loadTasks();
    });

    $('#btnResolvedTasks').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        filterTasks(true); // Filtrar tareas terminadas
    });

    $('#btnUnresolvedTasks').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        filterTasks(false); // Filtrar tareas pendientes
    });
});

function filterTasks(resolved) {
    const taskTable = document.getElementById('task-table').getElementsByTagName('tbody')[0];

    fetch(`${base_url}tasks/`)
        .then(response => response.json())
        .then(tasks => {
            taskTable.innerHTML = ''; // Limpiar contenido existente antes de agregar nuevas filas

            tasks.forEach(task => {
                if (resolved === 'all' || task.resolved === resolved) {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td>${task.id}</td>
                        <td>${task.title}</td>
                        <td>${task.description}</td>
                        <td>${task.resolved ? 'Terminado' : 'Pendiente'}</td>
                        <td>
                            <div class="d-flex justify-content-start">
                                <button type="button" class="btn btn-primary btn-sm mr-2" onclick="showEditModal(${task.id}, '${task.title}', '${task.description}', ${task.resolved})">Editar</button>
                                <button type="button" class="btn btn-danger btn-sm" onclick="showDeleteModal(${task.id})">Eliminar</button>
                            </div>
                        </td>
                    `;
                    taskTable.appendChild(row);
                }
            });
        })
        .catch(error => console.error('Error al cargar tareas:', error));
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    // Función para filtrar tareas por texto
    $('#searchText').on('keyup', function(){
        filterTasksByText();
    });

    function filterTasksByText() {
        var searchText = $('#searchText').val().toLowerCase();
        $('#task-table tbody tr').each(function(){
            var title = $(this).find('td:nth-child(2)').text().toLowerCase();
            var description = $(this).find('td:nth-child(3)').text().toLowerCase();
            if (title.includes(searchText) || description.includes(searchText)) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }
});
