# Api/urls.py
from django.urls import path
from .views import TaskListAPIView, UnresolvedTaskListAPIView, TaskCreateAPIView, ResolvedTaskListAPIView, UserTaskListAPIView, TaskDetailAPIView, UserListAPIView

urlpatterns = [
    # URL para listar todas las tareas
    path('tasks/', TaskListAPIView.as_view(), name='task-list'),

    # URL para listar todas las tareas sin resolver
    path('tasks/unresolved/', UnresolvedTaskListAPIView.as_view(), name='task-list-unresolved'),

    # URL para listar todas las tareas resueltas
    path('tasks/resolved/', ResolvedTaskListAPIView.as_view(), name='task-list-resolved'),

    # URL para listar todas las tareas de un usuario específico
    path('tasks/user/<int:user_id>/', UserTaskListAPIView.as_view(), name='user-task-list'),

    path('task/<int:id>/', TaskDetailAPIView.as_view(), name='task-detail'),

    path('tasks/create/', TaskCreateAPIView.as_view(), name='task-create'),

    # URL para listar todos los usuarios
    path('users/', UserListAPIView.as_view(), name='user-list'),
]
