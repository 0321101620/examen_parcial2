# api/api_consumer.py

import requests

base_url = 'http://localhost:8000/api/'

def get_tasks():
    response = requests.get(base_url + 'tasks/')
    if response.status_code == 200:
        return response.json()
    return []

def create_task(title, description, user_id):
    data = {
        'title': title,
        'description': description,
        'user': user_id,
        'resolved': False
    }
    response = requests.post(base_url + 'tasks/', data=data)
    return response.status_code == 201

def update_task(task_id, title, description, resolved):
    data = {
        'title': title,
        'description': description,
        'resolved': resolved
    }
    response = requests.put(base_url + f'tasks/{task_id}/', data=data)
    return response.status_code == 200

def delete_task(task_id):
    response = requests.delete(base_url + f'tasks/{task_id}/')
    return response.status_code == 204

if __name__ == '__main__':
    tasks = get_tasks()
    print("Todas las tareas:", tasks)

    created = create_task('Nueva tarea', 'Descripción de la nueva tarea', 1)
    print("Tarea creada:", created)

    updated = update_task(1, 'Tarea actualizada', 'Descripción actualizada', True)
    print("Tarea actualizada:", updated)

    deleted = delete_task(1)
    print("Tarea eliminada:", deleted)
