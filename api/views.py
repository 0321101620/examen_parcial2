from django.shortcuts import render
from rest_framework import generics
from .models import User, Task
from .serializers import UserSerializer, TaskSerializer
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

# Vista para listar todas las tareas
class TaskListAPIView(generics.ListAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

# Vista para listar todas las tareas sin resolver
class UnresolvedTaskListAPIView(generics.ListAPIView):
    queryset = Task.objects.filter(resolved=False)
    serializer_class = TaskSerializer

# Vista para listar todas las tareas resueltas
class ResolvedTaskListAPIView(generics.ListAPIView):
    queryset = Task.objects.filter(resolved=True)
    serializer_class = TaskSerializer

# Vista para listar todas las tareas de un usuario específico
class UserTaskListAPIView(generics.ListAPIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        user_id = self.kwargs['user_id']
        return Task.objects.filter(user_id=user_id)
    
class TaskDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    lookup_url_kwarg = 'id'

    def put(self, request, id, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


# Otra vista de ejemplo para listar usuarios
class UserListAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class TaskCreateAPIView(generics.CreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        # Asigna user_id por defecto aquí
        serializer.save(user_id=1)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
