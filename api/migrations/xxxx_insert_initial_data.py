# api/migrations/xxxx_insert_initial_data.py
from django.db import migrations

def create_initial_data(apps, schema_editor):
    User = apps.get_model('api', 'User')
    Task = apps.get_model('api', 'Task')

    # Crear usuarios de ejemplo
    user1 = User.objects.create(username='user1')
    user2 = User.objects.create(username='user2')

    # Crear tareas de ejemplo asignadas a usuarios
    Task.objects.create(title='Tarea 1', description='Descripción de la Tarea 1', resolved=False, user=user1)
    Task.objects.create(title='Tarea 2', description='Descripción de la Tarea 2', resolved=True, user=user1)
    Task.objects.create(title='Tarea 3', description='Descripción de la Tarea 3', resolved=False, user=user2)

class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),  # Ajusta según el nombre correcto de tu aplicación y migración previa
    ]

    operations = [
        migrations.RunPython(create_initial_data),
    ]

